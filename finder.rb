#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
  doc = Nokogiri::HTML(open("https://community.box.com/t5/How-to-Guides-for-Admins/Large-Scale-Deployments-Box-Sync/ta-p/6455",
	  "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36") )
rescue
  puts "Failed to fetch download page"
  exit 1
end

link = doc.xpath('//a').find {|x| x.inner_text.match(/Box Sync LTS Installer for Mac/) }

if link
    puts "#{link['href']}"
end